
<h1><u>Observations:</u></h1>
<h2>
1. Budget per student is not reflective of student performance<br>
2. Average reading scores are comparable across all schools and grades<br>
3. Huang high school has the highest budget, but is one of the bottom performing schools.<br>
</h2>


```python
# Imports
import pandas as pd
import numpy as np
```


```python
#set csv path
csvpath1 = ("schools_complete.csv")
csvpath2 = ("students_complete.csv")
print(csvpath1)
print(csvpath2)
```

    schools_complete.csv
    students_complete.csv



```python
schools_df = pd.read_csv(csvpath1)
students_df = pd.read_csv(csvpath2)
```


```python
students_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Student ID</th>
      <th>name</th>
      <th>gender</th>
      <th>grade</th>
      <th>school</th>
      <th>reading_score</th>
      <th>math_score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>Paul Bradley</td>
      <td>M</td>
      <td>9th</td>
      <td>Huang High School</td>
      <td>66</td>
      <td>79</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>Victor Smith</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>94</td>
      <td>61</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>Kevin Rodriguez</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>90</td>
      <td>60</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>Dr. Richard Scott</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>67</td>
      <td>58</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Bonnie Ray</td>
      <td>F</td>
      <td>9th</td>
      <td>Huang High School</td>
      <td>97</td>
      <td>84</td>
    </tr>
  </tbody>
</table>
</div>




```python
#students_df.info()
```


```python
schools_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>School ID</th>
      <th>name</th>
      <th>type</th>
      <th>size</th>
      <th>budget</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>Huang High School</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>Figueroa High School</td>
      <td>District</td>
      <td>2949</td>
      <td>1884411</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>Shelton High School</td>
      <td>Charter</td>
      <td>1761</td>
      <td>1056600</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>Hernandez High School</td>
      <td>District</td>
      <td>4635</td>
      <td>3022020</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Griffin High School</td>
      <td>Charter</td>
      <td>1468</td>
      <td>917500</td>
    </tr>
  </tbody>
</table>
</div>




```python
#schools_df.info()
```

* Mean - Average Math Score
* Mean - Average Reading Score
* Percentage via lambda? - % Passing Math
* Percentage via lambda? - % Passing Reading
* Mean of above two - Overall Passing Rate (Average of the above two)


<h2>District Summary</h2>


```python
schools_df.columns = ["School ID", "school", "School Type", "Size", "Budget"]
```


```python
schools_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>School ID</th>
      <th>school</th>
      <th>School Type</th>
      <th>Size</th>
      <th>Budget</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>Huang High School</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>Figueroa High School</td>
      <td>District</td>
      <td>2949</td>
      <td>1884411</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>Shelton High School</td>
      <td>Charter</td>
      <td>1761</td>
      <td>1056600</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>Hernandez High School</td>
      <td>District</td>
      <td>4635</td>
      <td>3022020</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Griffin High School</td>
      <td>Charter</td>
      <td>1468</td>
      <td>917500</td>
    </tr>
  </tbody>
</table>
</div>




```python
merge_df = pd.merge(students_df, schools_df, how="left", on="school")
```


```python
merge_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Student ID</th>
      <th>name</th>
      <th>gender</th>
      <th>grade</th>
      <th>school</th>
      <th>reading_score</th>
      <th>math_score</th>
      <th>School ID</th>
      <th>School Type</th>
      <th>Size</th>
      <th>Budget</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>Paul Bradley</td>
      <td>M</td>
      <td>9th</td>
      <td>Huang High School</td>
      <td>66</td>
      <td>79</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>Victor Smith</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>94</td>
      <td>61</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>Kevin Rodriguez</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>90</td>
      <td>60</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>Dr. Richard Scott</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>67</td>
      <td>58</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Bonnie Ray</td>
      <td>F</td>
      <td>9th</td>
      <td>Huang High School</td>
      <td>97</td>
      <td>84</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
    </tr>
  </tbody>
</table>
</div>




```python
#Total schools
total_schools = merge_df["school"].nunique()
#total_schools
```


```python
#Total students
total_students = merge_df["Student ID"].nunique()
```


```python
#Total budget
total_budget = schools_df["Budget"].sum()
```


```python
#list of schools
school = merge_df["school"].unique()
```


```python
#list of school types
school_type = merge_df.set_index(["school"]),["type"]
```

<h2><u>Math by Student</u></h2>


```python
#mean math score
average_math = merge_df["math_score"].mean()
```


```python
#filtering data by scores greater than 70/passing
passing_math = merge_df[merge_df["math_score"] > 70].count()["name"]
```


```python
#total number of math scores
total_math = merge_df["math_score"].count()
```


```python
#percentage passing math
percent_math = passing_math/(total_math)*100
```

<h2><u>Reading by Student</u></h2>


```python
#average reading
average_reading = merge_df["reading_score"].mean()
```


```python
#filtering by > 70 for passing reading
passing_reading = merge_df[merge_df["reading_score"] > 70].count()["name"]
```


```python
#total reading
total_reading = merge_df["reading_score"].count()
```


```python
#percent reading passed
percent_reading = passing_reading/(total_reading)*100
```

<h2><u>Overall Passing Rate by Student</u></h2>


```python
#passing rate overall by student
passing_rate_by_student = round((average_math + average_reading)/(2),2)
```


```python
#passing_rate_by_student
```


```python
#summary data students
student_summary = {'Total Schools':[total_schools],
          'Total Students':[total_students], 
        'Average Math Score':[average_math], 
        'Average Reading Score':[average_reading], 
        '% Passing Math':[percent_math],
        '% Passing Reading':[percent_reading],
        '% Overall Passing Rate':[passing_rate_by_student]}

```


```python
student_summary_df = pd.DataFrame(student_summary)
```


```python
student_summary_df = student_summary_df[["Total Schools",
                         "Total Students", 
                         "Average Math Score",
                         "Average Reading Score",
                         "% Passing Math", 
                         "% Passing Reading",
                         "% Overall Passing Rate" ]]
```

<H1><u>District Summary</u></h1>


```python
student_summary_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Total Schools</th>
      <th>Total Students</th>
      <th>Average Math Score</th>
      <th>Average Reading Score</th>
      <th>% Passing Math</th>
      <th>% Passing Reading</th>
      <th>% Overall Passing Rate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>15</td>
      <td>39170</td>
      <td>78.985371</td>
      <td>81.87784</td>
      <td>72.392137</td>
      <td>82.971662</td>
      <td>80.43</td>
    </tr>
  </tbody>
</table>
</div>



<h1><u>School Summary</u></h1>


```python
#Total students
students_per_school = merge_df.groupby(["school"]).count()["name"]
```


```python
#Total school budget per school
budget_per_school = schools_df.groupby(["school"]).sum()["Budget"]
```


```python
#per student budget
per_student_budget = budget_per_school/students_per_school
```

<h2><u>Math by School</u></h2>


```python
#average math per student
math_per_school = merge_df.groupby(["school"]).mean()["math_score"]
#math_per_school
```


```python
#filtering by > 70 
merge_math = merge_df.loc[merge_df["math_score"] > 70]
passing_math_school = merge_math['school'].value_counts()
#passing_math_school
```


```python
#passing math by school
#math_per_school = passing_math_school.groupby(["school"]).count()["name"]
```


```python
#percentage passing by school
math_avg_school = passing_math_school/students_per_school*100
```

<h2><u>Reading by School</u></h2>


```python
#average reading per student
reading_per_school = merge_df.groupby(["school"]).mean()["reading_score"]
```


```python
#filtering by > 70 
merge_reading = merge_df.loc[merge_df["reading_score"] > 70]
passing_reading_school = merge_reading['school'].value_counts()
#passing_reading_school
```


```python
#passing reading by school
#reading_per_school = passing_reading_school_df.groupby(["school"]).count()["name"]
```


```python
#percentage passing by school
reading_avg_school = passing_reading_school/students_per_school*100
```

<i>That's it for the calculations on just math and just reading</i>


```python
#passing rate by school
passing_rate_by_school = round((math_per_school + reading_per_school)/(2),2)
```

<h2><u>School Metrics</u></h2>


```python
#school type
school_type = schools_df.set_index(["school"])["School Type"]
```


```python
#summary data schools
school_summary = {'school': school,
                  'School Type': school_type,
                  'Total Students': students_per_school,
                  'Total School Budget':budget_per_school,
                  'Per Student Budget':per_student_budget,
                  'Average Math Score':math_avg_school,
                  'Average Reading Score':reading_avg_school,
                  '% Passing Math':passing_math_school,
                  '% Passing Reading':passing_reading_school,
                  'Overall Passing Rate':passing_rate_by_school
                 }
```


```python
school_summary_df = pd.DataFrame(school_summary)
#school_summary_df
```


```python
school_summary_df = school_summary_df[["School Type",
                                       "Total Students",
                                       "Total School Budget",
                                       "Per Student Budget",
                                       "Average Math Score",
                                       "Average Reading Score",
                                       "% Passing Math",
                                       "% Passing Reading",
                                       "Overall Passing Rate"]]
```


```python
school_summary_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>School Type</th>
      <th>Total Students</th>
      <th>Total School Budget</th>
      <th>Per Student Budget</th>
      <th>Average Math Score</th>
      <th>Average Reading Score</th>
      <th>% Passing Math</th>
      <th>% Passing Reading</th>
      <th>Overall Passing Rate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Bailey High School</th>
      <td>District</td>
      <td>4976</td>
      <td>3124928</td>
      <td>628.0</td>
      <td>64.630225</td>
      <td>79.300643</td>
      <td>3216</td>
      <td>3946</td>
      <td>79.04</td>
    </tr>
    <tr>
      <th>Cabrera High School</th>
      <td>Charter</td>
      <td>1858</td>
      <td>1081356</td>
      <td>582.0</td>
      <td>89.558665</td>
      <td>93.864370</td>
      <td>1664</td>
      <td>1744</td>
      <td>83.52</td>
    </tr>
    <tr>
      <th>Figueroa High School</th>
      <td>District</td>
      <td>2949</td>
      <td>1884411</td>
      <td>639.0</td>
      <td>63.750424</td>
      <td>78.433367</td>
      <td>1880</td>
      <td>2313</td>
      <td>78.93</td>
    </tr>
    <tr>
      <th>Ford High School</th>
      <td>District</td>
      <td>2739</td>
      <td>1763916</td>
      <td>644.0</td>
      <td>65.753925</td>
      <td>77.510040</td>
      <td>1801</td>
      <td>2123</td>
      <td>78.92</td>
    </tr>
    <tr>
      <th>Griffin High School</th>
      <td>Charter</td>
      <td>1468</td>
      <td>917500</td>
      <td>625.0</td>
      <td>89.713896</td>
      <td>93.392371</td>
      <td>1317</td>
      <td>1371</td>
      <td>83.58</td>
    </tr>
  </tbody>
</table>
</div>



<h2><u>Schools by Passing Rate</u></h2>


```python
#Top 5 schools
top_performing_df = school_summary_df.sort_values("Overall Passing Rate", ascending=False)
top_performing_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>School Type</th>
      <th>Total Students</th>
      <th>Total School Budget</th>
      <th>Per Student Budget</th>
      <th>Average Math Score</th>
      <th>Average Reading Score</th>
      <th>% Passing Math</th>
      <th>% Passing Reading</th>
      <th>Overall Passing Rate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Pena High School</th>
      <td>Charter</td>
      <td>962</td>
      <td>585858</td>
      <td>609.0</td>
      <td>91.683992</td>
      <td>92.203742</td>
      <td>882</td>
      <td>887</td>
      <td>83.94</td>
    </tr>
    <tr>
      <th>Wright High School</th>
      <td>Charter</td>
      <td>1800</td>
      <td>1049400</td>
      <td>583.0</td>
      <td>90.277778</td>
      <td>93.444444</td>
      <td>1625</td>
      <td>1682</td>
      <td>83.82</td>
    </tr>
    <tr>
      <th>Holden High School</th>
      <td>Charter</td>
      <td>427</td>
      <td>248087</td>
      <td>581.0</td>
      <td>90.632319</td>
      <td>92.740047</td>
      <td>387</td>
      <td>396</td>
      <td>83.81</td>
    </tr>
    <tr>
      <th>Thomas High School</th>
      <td>Charter</td>
      <td>1635</td>
      <td>1043130</td>
      <td>638.0</td>
      <td>90.214067</td>
      <td>92.905199</td>
      <td>1475</td>
      <td>1519</td>
      <td>83.63</td>
    </tr>
    <tr>
      <th>Wilson High School</th>
      <td>Charter</td>
      <td>2283</td>
      <td>1319574</td>
      <td>578.0</td>
      <td>90.932983</td>
      <td>93.254490</td>
      <td>2076</td>
      <td>2129</td>
      <td>83.63</td>
    </tr>
  </tbody>
</table>
</div>




```python
#Bottom 5 schools
bottom_performing_df = school_summary_df.sort_values("Overall Passing Rate", ascending=True)
bottom_performing_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>school</th>
      <th>School Type</th>
      <th>Total Students</th>
      <th>Total School Budget</th>
      <th>Per Student Budget</th>
      <th>Average Math Score</th>
      <th>Average Reading Score</th>
      <th>% Passing Math</th>
      <th>% Passing Reading</th>
      <th>Overall Passing Rate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Rodriguez High School</th>
      <td>Wright High School</td>
      <td>District</td>
      <td>3999</td>
      <td>2547363</td>
      <td>637.0</td>
      <td>64.066017</td>
      <td>77.744436</td>
      <td>2562</td>
      <td>3109</td>
      <td>78.79</td>
    </tr>
    <tr>
      <th>Huang High School</th>
      <td>Bailey High School</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
      <td>655.0</td>
      <td>63.318478</td>
      <td>78.813850</td>
      <td>1847</td>
      <td>2299</td>
      <td>78.91</td>
    </tr>
    <tr>
      <th>Ford High School</th>
      <td>Hernandez High School</td>
      <td>District</td>
      <td>2739</td>
      <td>1763916</td>
      <td>644.0</td>
      <td>65.753925</td>
      <td>77.510040</td>
      <td>1801</td>
      <td>2123</td>
      <td>78.92</td>
    </tr>
    <tr>
      <th>Figueroa High School</th>
      <td>Shelton High School</td>
      <td>District</td>
      <td>2949</td>
      <td>1884411</td>
      <td>639.0</td>
      <td>63.750424</td>
      <td>78.433367</td>
      <td>1880</td>
      <td>2313</td>
      <td>78.93</td>
    </tr>
    <tr>
      <th>Johnson High School</th>
      <td>Holden High School</td>
      <td>District</td>
      <td>4761</td>
      <td>3094650</td>
      <td>650.0</td>
      <td>63.852132</td>
      <td>78.281874</td>
      <td>3040</td>
      <td>3727</td>
      <td>79.02</td>
    </tr>
  </tbody>
</table>
</div>




```python
#merge summary and merge_df to include grade of student
final_summary_df = pd.merge(merge_df, school_summary_df, how="left", on="school")
final_summary_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Student ID</th>
      <th>name</th>
      <th>gender</th>
      <th>grade</th>
      <th>school</th>
      <th>reading_score</th>
      <th>math_score</th>
      <th>School ID</th>
      <th>School Type_x</th>
      <th>Size</th>
      <th>Budget</th>
      <th>School Type_y</th>
      <th>Total Students</th>
      <th>Total School Budget</th>
      <th>Per Student Budget</th>
      <th>Average Math Score</th>
      <th>Average Reading Score</th>
      <th>% Passing Math</th>
      <th>% Passing Reading</th>
      <th>Overall Passing Rate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>Paul Bradley</td>
      <td>M</td>
      <td>9th</td>
      <td>Huang High School</td>
      <td>66</td>
      <td>79</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
      <td>District</td>
      <td>4976</td>
      <td>3124928</td>
      <td>628.0</td>
      <td>64.630225</td>
      <td>79.300643</td>
      <td>3216</td>
      <td>3946</td>
      <td>79.04</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>Victor Smith</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>94</td>
      <td>61</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
      <td>District</td>
      <td>4976</td>
      <td>3124928</td>
      <td>628.0</td>
      <td>64.630225</td>
      <td>79.300643</td>
      <td>3216</td>
      <td>3946</td>
      <td>79.04</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>Kevin Rodriguez</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>90</td>
      <td>60</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
      <td>District</td>
      <td>4976</td>
      <td>3124928</td>
      <td>628.0</td>
      <td>64.630225</td>
      <td>79.300643</td>
      <td>3216</td>
      <td>3946</td>
      <td>79.04</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>Dr. Richard Scott</td>
      <td>M</td>
      <td>12th</td>
      <td>Huang High School</td>
      <td>67</td>
      <td>58</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
      <td>District</td>
      <td>4976</td>
      <td>3124928</td>
      <td>628.0</td>
      <td>64.630225</td>
      <td>79.300643</td>
      <td>3216</td>
      <td>3946</td>
      <td>79.04</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Bonnie Ray</td>
      <td>F</td>
      <td>9th</td>
      <td>Huang High School</td>
      <td>97</td>
      <td>84</td>
      <td>0</td>
      <td>District</td>
      <td>2917</td>
      <td>1910635</td>
      <td>District</td>
      <td>4976</td>
      <td>3124928</td>
      <td>628.0</td>
      <td>64.630225</td>
      <td>79.300643</td>
      <td>3216</td>
      <td>3946</td>
      <td>79.04</td>
    </tr>
  </tbody>
</table>
</div>




```python
#math scores by grade
by_grade_df = final_summary_df.sort_values(["school", "grade"], ascending=False)
```


```python
by_grade_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Student ID</th>
      <th>name</th>
      <th>gender</th>
      <th>grade</th>
      <th>school</th>
      <th>reading_score</th>
      <th>math_score</th>
      <th>School ID</th>
      <th>School Type_x</th>
      <th>Size</th>
      <th>Budget</th>
      <th>School Type_y</th>
      <th>Total Students</th>
      <th>Total School Budget</th>
      <th>Per Student Budget</th>
      <th>Average Math Score</th>
      <th>Average Reading Score</th>
      <th>% Passing Math</th>
      <th>% Passing Reading</th>
      <th>Overall Passing Rate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>24245</th>
      <td>24245</td>
      <td>Eric Espinoza</td>
      <td>M</td>
      <td>9th</td>
      <td>Wright High School</td>
      <td>86</td>
      <td>74</td>
      <td>10</td>
      <td>Charter</td>
      <td>1800</td>
      <td>1049400</td>
      <td>District</td>
      <td>3999</td>
      <td>2547363</td>
      <td>637.0</td>
      <td>64.066017</td>
      <td>77.744436</td>
      <td>2562</td>
      <td>3109</td>
      <td>78.79</td>
    </tr>
    <tr>
      <th>24250</th>
      <td>24250</td>
      <td>Christopher Newton</td>
      <td>M</td>
      <td>9th</td>
      <td>Wright High School</td>
      <td>82</td>
      <td>76</td>
      <td>10</td>
      <td>Charter</td>
      <td>1800</td>
      <td>1049400</td>
      <td>District</td>
      <td>3999</td>
      <td>2547363</td>
      <td>637.0</td>
      <td>64.066017</td>
      <td>77.744436</td>
      <td>2562</td>
      <td>3109</td>
      <td>78.79</td>
    </tr>
    <tr>
      <th>24256</th>
      <td>24256</td>
      <td>Sierra Martinez</td>
      <td>F</td>
      <td>9th</td>
      <td>Wright High School</td>
      <td>86</td>
      <td>86</td>
      <td>10</td>
      <td>Charter</td>
      <td>1800</td>
      <td>1049400</td>
      <td>District</td>
      <td>3999</td>
      <td>2547363</td>
      <td>637.0</td>
      <td>64.066017</td>
      <td>77.744436</td>
      <td>2562</td>
      <td>3109</td>
      <td>78.79</td>
    </tr>
    <tr>
      <th>24261</th>
      <td>24261</td>
      <td>Judy King</td>
      <td>F</td>
      <td>9th</td>
      <td>Wright High School</td>
      <td>78</td>
      <td>99</td>
      <td>10</td>
      <td>Charter</td>
      <td>1800</td>
      <td>1049400</td>
      <td>District</td>
      <td>3999</td>
      <td>2547363</td>
      <td>637.0</td>
      <td>64.066017</td>
      <td>77.744436</td>
      <td>2562</td>
      <td>3109</td>
      <td>78.79</td>
    </tr>
    <tr>
      <th>24278</th>
      <td>24278</td>
      <td>Jonathan Wilson</td>
      <td>M</td>
      <td>9th</td>
      <td>Wright High School</td>
      <td>77</td>
      <td>86</td>
      <td>10</td>
      <td>Charter</td>
      <td>1800</td>
      <td>1049400</td>
      <td>District</td>
      <td>3999</td>
      <td>2547363</td>
      <td>637.0</td>
      <td>64.066017</td>
      <td>77.744436</td>
      <td>2562</td>
      <td>3109</td>
      <td>78.79</td>
    </tr>
  </tbody>
</table>
</div>




```python
ninth = by_grade_df[by_grade_df["grade"]=="9th"]
```


```python
ninth_by_school = ninth.groupby(["school"]).mean()
```


```python
ninth_by_school_math = ninth_by_school["math_score"]
```


```python
tenth = by_grade_df[by_grade_df["grade"]=="10th"]
```


```python
tenth_by_school = tenth.groupby(["school"]).mean()
```


```python
tenth_by_school_math = tenth_by_school["math_score"]
```


```python
eleventh = by_grade_df[by_grade_df["grade"]=="11th"]
```


```python
eleventh_by_school = eleventh.groupby(["school"]).mean()
```


```python
eleventh_by_school_math = eleventh_by_school["math_score"]
```


```python
twelth = by_grade_df[by_grade_df["grade"]=="12th"]
```


```python
twelth_by_school = twelth.groupby(["school"]).mean()
```


```python
twelth_by_school_math = twelth_by_school["math_score"]
```


```python
grade_math_comb = {'school':school,
                  '9th Grade': ninth_by_school_math,
                  '10th Grade': tenth_by_school_math,
                  '11th Grade': eleventh_by_school_math,
                  '12th Grade': twelth_by_school_math}
```


```python
grade_math_comb_df = pd.DataFrame(grade_math_comb)
```


```python
grade_math_comb_df = grade_math_comb_df[['9th Grade',
                                         '10th Grade',
                                         '11th Grade',
                                         '12th Grade']]
```


```python
grade_math_comb_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>9th Grade</th>
      <th>10th Grade</th>
      <th>11th Grade</th>
      <th>12th Grade</th>
    </tr>
    <tr>
      <th>school</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Bailey High School</th>
      <td>77.083676</td>
      <td>76.996772</td>
      <td>77.515588</td>
      <td>76.492218</td>
    </tr>
    <tr>
      <th>Cabrera High School</th>
      <td>83.094697</td>
      <td>83.154506</td>
      <td>82.765560</td>
      <td>83.277487</td>
    </tr>
    <tr>
      <th>Figueroa High School</th>
      <td>76.403037</td>
      <td>76.539974</td>
      <td>76.884344</td>
      <td>77.151369</td>
    </tr>
    <tr>
      <th>Ford High School</th>
      <td>77.361345</td>
      <td>77.672316</td>
      <td>76.918058</td>
      <td>76.179963</td>
    </tr>
    <tr>
      <th>Griffin High School</th>
      <td>82.044010</td>
      <td>84.229064</td>
      <td>83.842105</td>
      <td>83.356164</td>
    </tr>
  </tbody>
</table>
</div>




```python
#reading scores by grade
```


```python
read_9 = by_grade_df[by_grade_df["grade"]=="9th"]
```


```python
read_9_by_school = read_9.groupby(["school"]).mean()
```


```python
read_9_by_school_reading = read_9_by_school["reading_score"]
```


```python
read_10 = by_grade_df[by_grade_df["grade"]=="10th"]
```


```python
read_10_by_school = read_10.groupby(["school"]).mean()
```


```python
read_10_by_school_reading = read_10_by_school["reading_score"]
```


```python
read_11 = by_grade_df[by_grade_df["grade"]=="11th"]
```


```python
read_11_by_school = read_11.groupby(["school"]).mean()
```


```python
read_11_by_school_reading = read_11_by_school["reading_score"]
```


```python
read_12 = by_grade_df[by_grade_df["grade"]=="12th"]
```


```python
read_12_by_school = read_12.groupby(["school"]).mean()
```


```python
read_12_by_school_reading = read_12_by_school["reading_score"]
```


```python
grade_read_comb = {'school':school,
                   '9th Grade':read_9_by_school_reading,
                   '10th Grade':read_10_by_school_reading,
                   '11th Grade':read_11_by_school_reading,
                   '12th Grade':read_12_by_school_reading}
```


```python
grade_read_comb_df = pd.DataFrame(grade_read_comb)
```


```python
grade_read_comb_df = grade_read_comb_df[['9th Grade',
                                         '10th Grade',
                                         '11th Grade',
                                         '12th Grade']]
```


```python
grade_read_comb_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>9th Grade</th>
      <th>10th Grade</th>
      <th>11th Grade</th>
      <th>12th Grade</th>
    </tr>
    <tr>
      <th>school</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Bailey High School</th>
      <td>81.303155</td>
      <td>80.907183</td>
      <td>80.945643</td>
      <td>80.912451</td>
    </tr>
    <tr>
      <th>Cabrera High School</th>
      <td>83.676136</td>
      <td>84.253219</td>
      <td>83.788382</td>
      <td>84.287958</td>
    </tr>
    <tr>
      <th>Figueroa High School</th>
      <td>81.198598</td>
      <td>81.408912</td>
      <td>80.640339</td>
      <td>81.384863</td>
    </tr>
    <tr>
      <th>Ford High School</th>
      <td>80.632653</td>
      <td>81.262712</td>
      <td>80.403642</td>
      <td>80.662338</td>
    </tr>
    <tr>
      <th>Griffin High School</th>
      <td>83.369193</td>
      <td>83.706897</td>
      <td>84.288089</td>
      <td>84.013699</td>
    </tr>
  </tbody>
</table>
</div>




```python
#bin ranges - bin by spending per student & return avg math, avg reading, % math, % reading, passing rate
spending_per_student_bins = [585, 615, 645, 675]
#I've spent too much time on this, and am so far behind that I need to stop now. I will do binning some other time
```


```python
#bin ranges same as above by school size
```


```python
#same as above, by school type
```


```python
#to submit: include notebook. Include README.md that is an exp9orted markdown version
#include written description of 3 observable trends
```
